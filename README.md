#** System Emulator **#

#### System consists of TCP Server and TCP Client. ####
---
#### Technologies: Linux, Python 3.5, PostgreSQL, Docker ####


##** TCP Server **##
Listening to TCP connections, receives package from client and saves to DB:

* list of clients
* client status (ON/OFF)
* received packages

###** Installation **###
Install [Docker](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/).

Enter server directory:
```
$ cd server/
```

Build services:
```
$ docker-compose build
```

Build, (re)create, start, and attache to containers for a service:
```
$ docker-compose up
```
---
##** TCP Client **##
Connects to the TCP server, sends a random package every N minutes.

###** Installation **###
Enter client directory:
```
$ cd client/
```

Install requirements:
```
$ pip install requirements.txt
```

Set server configuration and frequency of sending packages in config.json file:
```json
{
  "server_IP":"127.0.0.1",
  "server_port":5555,
  "period_n_sec":5
}
```

Run client:
```
$ python client.py
```
