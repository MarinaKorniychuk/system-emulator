import json
import logging

import asyncio
from string import ascii_letters, digits
from random import choice, randint
from uuid import getnode as get_mac

DEBUG = True


def generate_mac():
    mac = [0x00, 0x16, 0x3e,
           randint(0x00, 0x7f),
           randint(0x00, 0xff),
           randint(0x00, 0xff)
           ]
    return ''.join(map(lambda x: "%02x" % x, mac))


def generate_package():
    return ''.join(choice(ascii_letters + digits) for _ in range(20))


class Client(object):

    def __init__(self, server_ip, server_port, n):
        self.loop = asyncio.get_event_loop()
        self.server_ip = server_ip
        self.server_port = server_port
        self.n = n
        if DEBUG:
            self.mac = generate_mac()
        else:
            self.mac = get_mac()

    def start(self):
        self.loop.run_until_complete(self.tcp_client())

    def stop(self):
        self.loop.close()

    @asyncio.coroutine
    def tcp_client(self):
        while True:
            reader, writer = yield from asyncio.open_connection(self.server_ip, self.server_port, loop=self.loop)
            package = '{}*{}'.format(self.mac, generate_package())
            logging.info('Send: {}'.format(package))
            writer.write(package.encode())
            writer.close()
            yield from asyncio.sleep(self.n)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)

    with open('config.json') as file:
        config = json.loads(file.read())
    client = Client(
        config['server_IP'],
        config['server_port'],
        config['period_n_sec']
    )
    try:
        client.start()
    except KeyboardInterrupt:
        pass
    finally:
        client.stop()
