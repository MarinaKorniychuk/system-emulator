CREATE TABLE clients (
	id serial PRIMARY KEY,
	status varchar(3) NOT NULL,
	mac_address varchar(20) NOT NULL

);
CREATE TABLE packages (
	id serial PRIMARY KEY,
	client integer REFERENCES clients NOT NULL,
	data varchar(256) NOT NULL,
	sent_at timestamp DEFAULT now() NOT NULL
);
