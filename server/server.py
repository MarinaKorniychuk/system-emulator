import logging
import os

import asyncio
from datetime import timedelta, datetime

import psycopg2
from concurrent import futures


class Server(object):
    ''' Server Emulator '''

    def __init__(self, host, port, loop=None):
        self.loop = loop or asyncio.get_event_loop()
        self.server_gen = asyncio.start_server(self.handle_connection, host, port, loop=loop)
        self.server = None
        self.conn = None

    def start(self):
        try:
            self.conn = psycopg2.connect(
                "dbname='{db_name}' user='{db_user}' password='{db_password}' host='db'".format(
                    db_name=os.environ['POSTGRES_DB'],
                    db_user=os.environ['POSTGRES_USER'],
                    db_password=os.environ['POSTGRES_PASSWORD']
                )
            )
            logging.info('Connection to db established.')
        except psycopg2.OperationalError as exc:
            logging.error(str(exc))
            logging.error('Failed to connect to database.')
            return

        self.server = self.loop.run_until_complete(self.server_gen)
        self.loop.run_until_complete(self.update_statuses())
        logging.info('Listening established on {0}'.format(self.server.sockets[0].getsockname()))
        self.loop.run_forever()

    def stop(self):
        self.server.close()
        self.loop.run_until_complete(self.server.wait_closed())
        self.loop.close()

    def add_entry_to_db(self, mac_addr, package):
        if self.conn:
            try:
                cursor = self.conn.cursor()
                cursor.execute("""SELECT * FROM clients WHERE mac_address = %s""", (mac_addr,))

                client = cursor.fetchone()

                if not client:
                    cursor.execute("""INSERT INTO clients (status, mac_address) VALUES ('ON', %s);""", (mac_addr,))
                    cursor.execute("""SELECT id FROM clients WHERE mac_address = %s""", (mac_addr,))
                    client = cursor.fetchone()

                client_id = client[0]

                cursor.execute("""INSERT INTO packages (client, data) VALUES (%s, %s)""",
                               (client_id, package))

                # logging.info('Insert {}, {} into packages'.format(client_id, package))
                self.conn.commit()
                cursor.close()
            except psycopg2.OperationalError as exc:
                logging.error(str(exc))

    @asyncio.coroutine
    def handle_connection(self, reader, writer):
        peername = writer.get_extra_info('peername')
        logging.info('Accepted connection from {}'.format(peername))
        while not reader.at_eof():
            try:
                data = yield from asyncio.wait_for(reader.readline(), timeout=10.0)
                mac_address, package = data.decode().split('*')
                self.add_entry_to_db(mac_address, package)
                logging.info('Received: %r' % package)
            except futures.TimeoutError:
                break
        writer.close()

    @asyncio.coroutine
    def update_statuses(self):
        if self.conn:
            while True:
                logging.info('Status Updating is running ...')
                try:
                    cursor = self.conn.cursor()
                    cursor.execute('SELECT client, MAX(sent_at) > %s AS is_active FROM packages GROUP BY client;',
                                   (datetime.now() - timedelta(minutes=15),))
                    clients = cursor.fetchall()
                    active_clients = [client[0] for client in clients if client[1]]
                    cursor.execute("UPDATE clients SET status = (CASE WHEN id <> ALL(%s) THEN 'ON' ELSE 'OFF' END);",
                                   [active_clients])

                    cursor.close()
                except psycopg2.OperationalError as exc:
                    logging.error(exc)

                yield from asyncio.sleep(900)


if __name__ == '__main__':

    logging.basicConfig(level=logging.DEBUG)
    server = Server('0.0.0.0', 80)
    try:
        server.start()
    except KeyboardInterrupt:
        pass
    finally:
        server.stop()
